import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

export interface IMFEHeaderComponentItem {
  label: string;
  href: string;
  target: string;
  clientRoute: boolean;
}

@Component({
  selector: 'mfe-header',
  templateUrl: 'mfe-header.component.html',
  styleUrls: ['mfe-header.component.css'],
})
export class MFEHeaderComponent implements AfterViewInit {
  @Input() items: IMFEHeaderComponentItem[] =  [
    {label: 'Home', href: '', target:'p', clientRoute: true},
    {label: 'About', href: 'about', target:'p', clientRoute: true},
    {label: 'Services', href: 'services', target:'p', clientRoute: true},
    {label: 'Contact', href: 'contact', target:'p', clientRoute: true}
  ];

  @Input() item: string;
  @Input() notification = [];

  ngAfterViewInit()
  {
  }

  clickHandler(event: MouseEvent, route)
  {
    event.preventDefault()
    let events = new CustomEvent('mfeHeader_item_clicked', {  bubbles: true, detail: route });
    event.currentTarget.dispatchEvent(events);
  }
}