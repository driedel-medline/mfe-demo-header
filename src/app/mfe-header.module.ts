import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, Injector, NgModule, NgZone, NO_ERRORS_SCHEMA } from '@angular/core';
import { MFEHeaderComponent } from './mfe-header.component';
import { createCustomElement } from '@angular/elements';

// To run local app in browser in isolation
@NgModule({
  imports: [
    BrowserModule
  ],
  declarations: [
    MFEHeaderComponent
  ],
  providers: [],
  exports: [
    MFEHeaderComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA
  ],
  bootstrap: [],
  entryComponents: [MFEHeaderComponent]
})
export class MFEHeaderModule {
  // IMPORTANT!!!!! Angular micro frontends can't use multiple instances of zone js, so we need to create a SINGLE instance that will be used by all!
  // So we need to attach zone to the window object, if it doesn't exist already, if it does, we'll use that. See bootstrap.ts
  attachNgZone()
  {
    const win = (window as any);

    if(!win.ngZone)
    {
      win.ngZone = this.ngZone;
    }
  }

  constructor(injector: Injector, private ngZone: NgZone) {
    this.attachNgZone();

    const customElement = createCustomElement(MFEHeaderComponent, {injector: injector});
    customElements.define('mfe-header', customElement);
  }

  ngDoBootstrap(){}
}
