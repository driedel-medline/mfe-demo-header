import { MFEHeaderModule } from './app/mfe-header.module';
import { environment } from './environments/environment';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';

if (environment.production) {
  enableProdMode();
}


// IMPORTANT!!!!! Angular micro frontends can't use multiple instances of zone js, so we need to create a SINGLE instance that will be used by all!
// So we need to attach zone to the window object, if it doesn't exist already, if it does, we'll use that. See app.module.ts
if((window as any).ngZone)
{
  platformBrowserDynamic().bootstrapModule(MFEHeaderModule, { ngZone: (window as any).ngZone })
  .catch(err => console.error(err));
}
else
{
  platformBrowserDynamic().bootstrapModule(MFEHeaderModule)
  .catch(err => console.error(err));
}
