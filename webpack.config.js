const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");

module.exports = {
  optimization: {
    // Only needed to bypass a temporary bug
    runtimeChunk: false
  },
  entry: {
    /*
      Required for the remoteEntry.js file of this micro frontend.  It allows for it's version of require js to load
      bundles local to the remoteEntry file, despite the code being run within a consuming host application's context.
    */
    mfeHeader: "./src/public-path.js"
  },

  output: {
    "uniqueName": "mfeHeader"
  },
  plugins: [
    new ModuleFederationPlugin({
      name: "mfeHeader",

      filename: "remoteEntry.js",
      exposes: {
        // For use if this were a remote module we could route to
        './MFEHeaderModule': './src/app/mfe-header.module.ts',

        // For use when we're using the component just as a remote angular component (vs a web component)
        './MFEHeaderComponent': './src/app/mfe-header.component.ts',

        // For use when the MFE is a web component, we need to package the platform-dynamic lib with source
        './MFEHeaderPolyfills': './src/polyfills.ts',

        // For use when the MFE is a web component, we need to package the platform-dynamic lib with source
        './MFEHeaderBootstrap': './src/bootstrap.ts'
      },
      shared: {
        "@angular/core": { singleton: false },
        "@angular/common": { singleton: false },
        "@angular/elements": { singleton: false },
        "@angular/compiler": { singleton: false },
        "@angular/platform-browser": {singleton: false },
        "@angular/platform-browser-dynamic": {singleton: false },
        "@webcomponents/webcomponentsjs": {singleton: false },
        "zone.js": { singleton: false }
      }
    }),
  ],
};

